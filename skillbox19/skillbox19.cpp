﻿#include <iostream>

class Animal {
public:
    virtual void Voice() {
        std::cout << "Muuu!" << std::endl;
    }
};

class Dog : public Animal {
public:
    void Voice() override {
        std::cout << "Woof!" << std::endl;
    }
};

class Cat : public Animal {
public:
    void Voice() override {
        std::cout << "Meow!" << std::endl;
    }
};

class Mouse : public Animal {
public:
    void Voice() override {
        std::cout << "Piep!" << std::endl;
    }
};

int main()
{
    Animal* dog = new Dog();
    Animal* cat = new Cat();
    Animal* mouse = new Mouse();
    Animal* animals[3] = { dog , cat, mouse };
    for (Animal* animal : animals) {
        animal->Voice();
    }
    //delete memory
    for (Animal* animal : animals) {
        delete animal;
    }
}